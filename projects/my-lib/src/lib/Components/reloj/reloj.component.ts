import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reloj',
  templateUrl: './reloj.component.html',
  styleUrls: ['./reloj.component.scss']
})
export class RelojComponent implements OnInit {

  private days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
  private months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  
  private date = new Date();

  public hour: any;
  public minutes: string;
  public seconds: string;
  public ampm: string;
  public day: string;
  public month: string;
  public fecha: any;

  constructor() { }

  ngOnInit(): void {

    setInterval(()=>{
      const date = new Date();
      this.updateDate(date);
    },1000); 

  }

  private updateDate(date: Date){

    const hours = date.getHours(); //Obtiene las horas
    this.ampm = hours >= 12 ? 'pm' : 'am'; //Verifica si es mayor a 12 se colocara PM sino sera AM. 
    this.hour = hours % 12; //Hace la hora en formato de 12 horas
    this.hour = hours ? this.hour: 12; //Si la hora es 0, se le asigna 12
    this.hour = this.hour < 10 ? '0' + this.hour : this.hour; //Si la hora es de un solo dígito, se agrega 0 delante de ella

    const minutes$ = date.getMinutes();//Obtiene los minutos
    this.minutes = minutes$ < 10 ? '0' + minutes$ : minutes$.toString(); //Si los minutos es de un solo dígito, se agrega 0 delante de ella

    const seconds$ = date.getSeconds();//Obtiene los segundos
    this.seconds = seconds$ < 10 ? '0' + seconds$ : seconds$.toString(); //Si los segundos es de un solo dígito, se agrega 0 delante de ella

    this.day = this.days[this.date.getDay()];

    const day = date.getDate(); //Dia
    this.month = this.months[this.date.getMonth()]; //Mes
    const year = date.getFullYear(); //Año
    
    this.fecha = day + ' de ' + ' ' + this.month + ' ' + year + '.';
 
  }

}
