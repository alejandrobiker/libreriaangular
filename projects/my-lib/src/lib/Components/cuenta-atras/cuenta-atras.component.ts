import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-cuenta-atras',
  templateUrl: './cuenta-atras.component.html',
  styleUrls: ['./cuenta-atras.component.scss']
})
export class CuentaAtrasComponent implements OnInit {

  hour:number=0;
  minutes:number=0;
  segunds:number=0;
  onOff:boolean=true;
  contador:number;
  finish:string;
  error:string = "";
  validar:boolean=true;


  constructor() { }

  ngOnInit(): void {
  }

  ngOnChange():void{
  }

  iniciar(){
    if(!this.onOff){
      this.onOff=true;
      clearInterval(this.contador);
    }else{ 
      this.onOff=false;
      this.finish = "";
      this.error = "";
      this.contador = setInterval(()=>{
        this.temporadizador()
      },1000)
    }
  }

  temporadizador(){
    if(this.segunds == 0 && this.minutes == 0 && this.hour == 0){
      clearInterval(this.contador);
      this.onOff=true;
      this.finish = "¡Tiempo finalizado!";
    }else{
      if(this.segunds > 59 || this.segunds < 0 || this.minutes > 60 || this.minutes < 0 || this.hour > 24 || this.hour < 0){
        clearInterval(this.contador);
        this.error = "Error, campos incorrectos.";
        this.onOff=true;
        this.hour = 0;
        this.minutes = 0;
        this.segunds = 0;
        this.validar=false;
      }
      if(this.validar){
        if( this.minutes == 0 && this.segunds == 0){
          this.hour-=1;
          this.minutes=59;
          this.segunds=59;
        }
        if(this.segunds == 0){
          this.minutes -= 1;
          this.segunds = 59;
        }
        this.segunds -= 1;
      }
      this.validar=true;
    }
  }

  detener(){
    clearInterval(this.contador);
    this.hour = 0;
    this.minutes = 0;
    this.segunds = 0;

    this.contador = null;
    this.onOff = true;
    this.finish = "";
    this.error = "";
    this.validar = true;
  }
  


}
