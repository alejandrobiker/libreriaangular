import { Component, OnInit } from '@angular/core';
import { interval, timer } from 'rxjs';

@Component({
  selector: 'app-cronometro',
  templateUrl: './cronometro.component.html',
  styleUrls: ['./cronometro.component.scss']
})
export class CronometroComponent implements OnInit {

  contador:any;
  hour: number = 0;
  minutes: number = 0;
  segunds: number = 0;
  onOff:boolean=true;

  lapsoTimer: Array<any> = [];

  constructor() { }

  ngOnInit(): void {
  
  }

  iniciar(){
    if(!this.onOff){
      this.onOff=true;
      clearInterval(this.contador);
    }else{     
      this.onOff = false; 
      this.contador = setInterval(() => {
          this.segunds += 1;
          if(this.segunds == 60){
            this.segunds = 0;
            this.minutes += 1;
            if(this.minutes == 60){
              this.minutes = 0;
              this.hour += 1;
            }
          }
        },1000)
    }
  }

  lapso(){
    let obj:any = {};
    obj.hour = this.hour;
    obj.minutes = this.minutes;
    obj.segunds = this.segunds;

    this.lapsoTimer.push(obj);
  }

  detener(){
    clearInterval(this.contador);
    this.hour= 0;
    this.minutes =0;
    this.segunds = 0;

    this.contador = null;
    this.lapsoTimer = [];
    this.onOff = true;

  }

}
