import { NgModule } from '@angular/core';
import { MyLibComponent } from './my-lib.component';
import { Routes, RouterModule } from '@angular/router';
import { RelojComponent } from './Components/reloj/reloj.component';
import { CronometroComponent } from './Components/cronometro/cronometro.component';
import { CuentaAtrasComponent } from './Components/cuenta-atras/cuenta-atras.component';
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: 'reloj', component: RelojComponent },
  { path: 'cronometro', component: CronometroComponent },
  { path: 'cuenta-atras', component: CuentaAtrasComponent }
];

@NgModule({
  declarations: [
    MyLibComponent,
    RelojComponent,
    CronometroComponent,
    CuentaAtrasComponent      
  ],
  imports: [ 
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [MyLibComponent]
})
export class MyLibModule { }
